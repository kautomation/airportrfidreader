#!/bin/sh

echo "\033c"
echo "to stop it: [ kill $$ ]" 
now=$(date +"%I:%M:%S")
echo $now
/usr/bin/python /home/debian/airportrfidreader/termRearAsyncUpdate.py &
sleep 0.1
/usr/bin/python /home/debian/airportrfidreader/termRearAsync.py
