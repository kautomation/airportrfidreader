#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
using namespace std;
int main(int argc, char *argv[]) {
FILE *export_file = NULL;
FILE *IO_dir = NULL;
char str_low[] = "low";
char str_high[] = "high";
char str_port_in[] = "44";
char str_port_out[] = "45";

// Open Port
int count;  
  
    // Display each command-line argument.  
cout << argv[0] << " {in / out} { open / close }\n";  

if((strcmp(argv[1], "in")) == 0){
	export_file = fopen ("/sys/class/gpio/export", "w");
	fwrite (str_port_in, 1, sizeof(str_port_in), export_file);
	fclose (export_file);

	if((strcmp(argv[2], "open")) == 0){
        	cout << "open" << "\n";
        	IO_dir = fopen ("/sys/class/gpio/gpio44/direction", "w");
        	fwrite (str_high, 1, sizeof(str_high), IO_dir);   // pin = HIGH
        	fclose (IO_dir);
	}
	if((strcmp(argv[2], "close")) == 0){
        	cout << "close" << "\n";
        	IO_dir = fopen ("/sys/class/gpio/gpio44/direction", "w");
        	fwrite (str_low, 1, sizeof(str_low), IO_dir);   //pin = LOW
        	fclose (IO_dir);
	}

	}

if((strcmp(argv[1], "out")) == 0){
        export_file = fopen ("/sys/class/gpio/export", "w");
        fwrite (str_port_out, 1, sizeof(str_port_out), export_file);
        fclose (export_file);

        if((strcmp(argv[2], "open")) == 0){
                cout << "open" << "\n";
                IO_dir = fopen ("/sys/class/gpio/gpio45/direction", "w");
                fwrite (str_high, 1, sizeof(str_high), IO_dir);   // pin = HIGH
                fclose (IO_dir);
        }
        if((strcmp(argv[2], "close")) == 0){
                cout << "close" << "\n";
                IO_dir = fopen ("/sys/class/gpio/gpio45/direction", "w");
                fwrite (str_low, 1, sizeof(str_low), IO_dir);   //pin = LOW
                fclose (IO_dir);
        }

	}

}

