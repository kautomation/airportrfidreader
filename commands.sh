
#write out current crontab
crontab -l > restartcron
#echo new cron into cron file

echo "*/5 * * * * sudo systemctl restart messaging.service" >> restartcron
echo "*/30 * * * * sudo systemctl restart persistance.service" >> restartcron
echo "0 */4 * * * sudo reboot" >> restartcron
crontab restartcron
rm restartcron

su debian  
cd /home/debian
wget https://ruh9900.s3.eu-west-2.amazonaws.com/Python-3.6.9.tar.gz
tar -xzvf Python-3.6.9.tar.gz
cd Python-3.6.9
sudo make altinstall


python3.6 -m venv /home/debian/venv
source venv/bin/activate
python3.6 -m pip install socketIO-client