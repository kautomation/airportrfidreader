import os
import time
import json, requests
import sqlite3

data = []

while True:
    db = sqlite3.connect('/home/debian/airportrfidreader/termdb')
    cursor = db.cursor()
    try:
        cursor.execute('''CREATE TABLE IF NOT EXISTS cars(id INTEGER PRIMARY KEY, tag TEXT unique, direction TEXT)''')
        db.commit()
    except:
        pass


    data = []
    try:
        rfids=os.popen("/home/debian/airportrfidreader/readasync tmr://0.0.0.0 --ant 1,2")
        for rfid in rfids:
            print rfid.rstrip("\n")
            data.append(rfid.rstrip("\n"))
    except:
        print "error"

    flag = True
    num = 0
    for num in range(len(data)):
        print num
        if flag:
            try:
                cursor.execute('''INSERT INTO cars(tag, direction) VALUES(?,?)''', (data[num], data[num+1]))
                db.commit()
            except:
                pass
        flag = not flag

    cursor.execute('''SELECT tag, direction FROM cars''')
    all_rows = cursor.fetchall()
    for row in all_rows:
        print('{0} : {1}'.format(row[0], row[1]))

    db.close()

    
