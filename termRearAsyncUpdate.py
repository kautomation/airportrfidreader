import time
import json, requests
import os
import sqlite3

db = sqlite3.connect('/home/debian/airportrfidreader/termdb')
#db = sqlite3.connect(':memory:')
cursor = db.cursor()
try:
    cursor.execute('''CREATE TABLE IF NOT EXISTS cars(id INTEGER PRIMARY KEY, tag TEXT unique, direction TEXT)''')
    db.commit()
except:
    pass


terminal_no = '2'
url_health     = 'https://nparking.net/api/terminal/health'
url_in         = 'https://nparking.net/api/terminal/in'
url_out        = 'https://nparking.net/api/terminal/out'
url_in_command = 'https://nparking.net/api/terminal/button/in'
headers        = {'Content-type': 'application/json'}

while True:
   
    try:
        cursor.execute('''SELECT tag, direction FROM cars''')
        all_rows = cursor.fetchall()
        for row in all_rows:
            print('{0} : {1}'.format(row[0], row[1]))
            if row[1] == '1':
                print '#in#'
                data_in = json.dumps({'terminal_no' : terminal_no, 'rfid_id': row[0]})
                response_in = requests.post(url_in, data=data_in, headers=headers, verify=False)
                data_in = json.loads(response_in.text)
                print data_in
                print data_in['msg']
                if data_in['msg'] == 'open barier':
                    print 'open barier now'
                    os.popen('sudo /home/debian/airportrfidreader/GPIO in open')
                    print row[0] 
                    cursor.execute('''DELETE FROM cars WHERE tag = ? ''', (row[0],))
                    db.commit()

                if data_in['msg'] == 'not allowed':
                    print 'not allowed'
                    print row[0]
                    cursor.execute('''DELETE FROM cars WHERE tag = ? ''', (row[0],))
                    db.commit()
                if data_in['msg'] == 'error':
                    cursor.execute('''DELETE FROM cars WHERE tag = ? ''', (row[0],)) 
                    db.commit()
    except Exception, e:
        print e



    try:
        cursor.execute('''SELECT tag, direction FROM cars''')
        all_rows = cursor.fetchall()
        for row in all_rows:
            print('{0} : {1}'.format(row[0], row[1]))
            if row[1] == '2':
                print '#out#'
                data_out = json.dumps({'terminal_no' : terminal_no, 'rfid_id': row[0]})
                response_out = requests.post(url_out, data=data_out, headers=headers, verify=False)
                data_out = json.loads(response_out.text)
                print data_out
                print data_out['msg']
                if data_out['msg'] == 'open barier':
                    print 'open barier now'
                    os.popen('sudo /home/debian/airportrfidreader/GPIO out open')
                    print row[0]
                    cursor.execute('''DELETE FROM cars WHERE tag = ? ''', (row[0],))
                    db.commit()
    except Exception, e:
        print e



    try:
        print '#out from far command#'
        data_in_dis = json.dumps({'terminal_no' : terminal_no})
        response_in_dis = requests.post(url_in_command, data=data_in_dis, headers=headers, verify=False)
        data_in_dis = json.loads(response_in_dis.text)
        print data_in_dis
        print data_in_dis['msg']
        if data_in_dis['msg'] == 'open barier':
            print 'open barier now'
            os.popen('sudo /home/debian/airportrfidreader/GPIO in open')
    except Exception, e:
        print e




    try:
        data_health = json.dumps({'terminal_no' : terminal_no})
        health_msg = requests.post(url_health, data=data_health, headers=headers, verify=False)
        print health_msg.text
    except:
        print "error HEALTH request"
    try:
        os.popen('sudo /home/debian/airportrfidreader/GPIO in close') # entrance the gate barrier reset
        os.popen('sudo /home/debian/airportrfidreader/GPIO out close') # entrance the gate barrier reset

    except:
        pass


#    break
